# Synopsis

My main goal is a *strict* seperation between functions that do logic and functions that modify the game states. Thus, the concept of a `Mutation`, a concept from other popular libraries like Redux and Vuex. However, I reverse the concept by using mutations on the backend rather then the frontend.

Basic logic is like this:

1. A event happens, either the client clicked something or a timer ran out or anything.
2. A event handler gets a *read only* copy of the game state and the event.
3. The event handler produces any number of mutations.
4. Mutations are then applied to the game state to modify it.
5. Mutations are also sent to clients.

This has many advantages:

1. Event handlers are easily unit testable since you can check what mutations they produce.
2. Client and server synchonization is much easier to guarentee, since they use the same mutations system.

However, it also has some unique challanges:

1. Not every mutation needs to be sent to every client. Need a good way to filter which client gets which event efficiently.
     1. some mutations may need some fields censored, for example if another player draws a card you should know a card was drawn but not which one.
2. Efficiency is a bit hard since everything needs a (immutable) reference to the game state still. A more complex network of locks might solve the issue.

# Result

Server divided into these components:

1. Event dispachers -> Source of events, mostly from sockets
2. Event handlers -> Starts with a single handler that recursively delegates to more specific handlers for a specific type of event
3. Mutation handlers -> Apply mutation to the global state
4. Client Filters -> Determine which clients need to know about the mutation, might modify, add, or remove mutations before they are sent.
5. Mutation dispachers -> Send mutations to the clients.

# Switching screens

When loading the game for the first time or switching screens, client state needs to be updated by a lot at once. Mutations are still they way to do this,
a client state needs to be reduced into a series of mutations to get the client back up to date. THis will be handled by the client filter, since it's the component that can send mutations to only one specific client. It will delegate to the relavent data structures.

For example, when switching to the lobby page a series of `CreateLobby` events need to be generated.
