use crate::{ClientId, GameId, GameSettings};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct GameLobbyState {
    pub id: GameId,
    pub settings: GameSettings,
    pub players: Vec<ClientId>,
}

impl GameLobbyState {
    #[cfg(feature = "random")]
    pub fn new() -> Self {
        return Self {
            id: GameId::new(),
            settings: Default::default(),
            players: Default::default(),
        };
    }

    pub fn new_with_id(id: GameId) -> Self {
        return Self {
            id: id,
            settings: Default::default(),
            players: Default::default(),
        };
    }
}
