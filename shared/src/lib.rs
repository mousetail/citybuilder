use game_settings::GameId;

pub mod client_location;
pub mod client_state;
pub mod game_settings;
pub mod lobby_state;
pub mod mutation_error;
pub mod mutations;
pub mod versioned_cell;
pub mod events;
pub mod game;
pub mod lobby;

use crate::client_location::ClientLocation;
use crate::client_state::ClientId;
use crate::game_settings::GameSettings;
use serde::{Deserialize, Serialize};
use lobby::lobby_state::GameLobbyState;

#[derive(Serialize, Deserialize, Debug)]
pub struct LobbyMutation {
    pub game_id: GameId,
    pub mutation_type: LobbyMutationType,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum LobbyMutationType {
    AddRoom(GameLobbyState),
    Remove,
    UpdateRoomSettings(GameSettings),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum GameMutation {
    GameSettingsMutation(GameSettings),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Mutation {
    LobbyMutation(LobbyMutation),
    GameMutation {
        game_id: GameId,
        mutation: GameMutation,
    },
    ChatMessage(String),
    MoveClient {
        client: ClientId,
        destination: ClientLocation,
    },
    ClientError {
        client: ClientId,
        message: String,
    },
}
