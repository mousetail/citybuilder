use crate::lobby_state::LobbyState;
use crate::mutation_error::MutationError;
use crate::{LobbyMutation, LobbyMutationType};

pub fn apply_lobby_mutation(
    state: &mut LobbyState,
    mutation: &LobbyMutation,
) -> Result<(), MutationError> {
    match &mutation.mutation_type {
        LobbyMutationType::AddRoom(room) => {
            state.games.insert(mutation.game_id, room.clone());
        }
        LobbyMutationType::Remove => {
            state.games.remove(&mutation.game_id);
        }
        LobbyMutationType::UpdateRoomSettings(settings) => {
            state
                .games
                .get_mut(&mutation.game_id)
                .ok_or(MutationError::InvalidGame {
                    actual_game_id: Some(mutation.game_id),
                    expected_game_id: None,
                })?
                .settings = settings.clone();
        }
    };

    Ok(())
}
