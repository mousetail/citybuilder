use crate::client_state::ClientId;
use crate::GameId;
use std::collections::HashMap;
use crate::lobby::lobby_state::GameLobbyState;

#[derive(Default, PartialEq, Clone)]
pub struct LobbyState {
    pub games: HashMap<GameId, GameLobbyState>,
    pub players: Vec<ClientId>,
}
