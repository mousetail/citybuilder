use serde::{Deserialize, Serialize};

use crate::ClientLocation;
#[cfg(feature="random")]
use rand::RngCore;
use std::sync::{Arc, Mutex};

#[derive(Ord, PartialOrd, Eq, PartialEq, Copy, Clone, Hash, Serialize, Deserialize, Debug)]
pub struct ClientId {
    pub id: u32,
}

impl ClientId {
    #[cfg(feature="random")]
    pub fn new() -> Self {
        Self {
            id: rand::thread_rng().next_u32(),
        }
    }
}

impl From<u32> for ClientId {
    fn from(v: u32) -> Self {
        ClientId { id: v }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug, PartialEq)]
pub struct ClientState {
    pub location: ClientLocation,
    pub id: ClientId,
}

pub type PlayerStateRef = Arc<Mutex<ClientState>>;
