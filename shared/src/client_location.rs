use crate::GameId;
use serde::{Deserialize, Serialize};

#[derive(Eq, PartialEq, Serialize, Deserialize, Debug, Copy, Clone, Hash)]
pub enum ClientLocation {
    None,
    InGame(GameId),
    InGameLobby(GameId),
    InLobby,
}

impl ClientLocation {
    fn get_location_type(&self) -> ClientStateType {
        match self {
            Self::None => ClientStateType::None,
            Self::InGame(_) => ClientStateType::InGame,
            Self::InGameLobby(_) => ClientStateType::InLobby,
            Self::InLobby => ClientStateType::InLobby,
        }
    }
}

#[derive(Eq, PartialEq, Serialize, Deserialize, Debug, Copy, Clone, Hash)]
pub enum ClientStateType {
    None,
    InGame,
    InGameLobby,
    InLobby,
}
