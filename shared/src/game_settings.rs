#[cfg(feature = "random")]
use rand;
use serde::{Deserialize, Serialize, Serializer};
use std::fmt::Formatter;

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug, Serialize, Deserialize)]
pub struct GameId {
    id: u32,
}

impl GameId {
    #[cfg(feature = "random")]
    pub fn new() -> GameId {
        GameId { id: rand::random() }
    }

    pub fn from_string(string: &str) -> Self {
        let mut id: u32 = 0;
        let mut base: u64 = 1;
        for char in string.chars() {
            let value = if char.to_ascii_uppercase() >= 'A' {
                char.to_ascii_uppercase() as u8 - 'A' as u8
            } else {
                char as u8 - '0' as u8 + 26
            };

            id += (value as u32) * base as u32;
            base *= 36;
        }

        Self { id }
    }
}

impl std::fmt::Display for GameId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut string = String::new();
        let mut num = self.id;
        let power = 36;
        while num > 0 {
            let char = (num % power) as u8;
            if char < 26 {
                string.push(('a' as u8 + char) as char);
            } else {
                string.push(('0' as u8 + char - 26) as char);
            }
            num /= power;
        }
        f.collect_str(&string)
    }
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Copy, Clone)]
pub enum GameSpeed {
    Slow,
    Normal,
    Fast,
}

impl Default for GameSpeed {
    fn default() -> Self {
        GameSpeed::Normal
    }
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Copy, Clone)]
pub enum GameMode {
    Normal,
    ThreeCards,
}

impl Default for GameMode {
    fn default() -> Self {
        GameMode::Normal
    }
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Default, Clone)]
pub struct GameSettings {
    pub speed: GameSpeed,
    pub mode: GameMode,
}
