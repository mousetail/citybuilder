use serde::{Deserialize, Serialize};

use crate::{GameId, GameSettings};
use crate::events::game_events::GameEvent;

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub enum LobbyEvent {
    RequestRooms,
    JoinRoom(GameId),
    UpdateGameSettings(GameId, GameSettings),
    CreateRoom,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub enum ClientEvent {
    LobbyEvent(LobbyEvent),
    SendChatMessage(String),
    EnterLobby,
    EnterGameLobby(GameId),
    GameEvent(GameEvent),
}
