use crate::GameId;
use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Serialize, Deserialize, Debug, Eq, PartialEq)]
pub enum GameEvent {
    StartGame {
        game_id: GameId
    }
}
