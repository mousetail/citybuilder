use std::cell::{Ref, RefCell, RefMut};
use std::rc::Rc;

#[derive(Eq, PartialEq, Clone)]
pub struct VersionedCell<T> {
    value: Rc<RefCell<T>>,
    version: usize,
}

impl<T> VersionedCell<T> {
    pub fn new(value: T) -> Self {
        Self {
            value: Rc::new(RefCell::new(value)),
            version: 0,
        }
    }

    pub fn borrow(&self) -> Ref<T> {
        self.value.borrow()
    }

    pub fn borrow_mut(&mut self) -> RefMut<T> {
        self.version += 1;
        RefCell::borrow_mut(&self.value)
    }
}
