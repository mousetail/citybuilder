use crate::client_location::ClientStateType;
use crate::{ClientId, GameId};

#[derive(Debug)]
pub enum MutationError {
    InvalidPage {
        expected_page: ClientStateType,
        actual_page: ClientStateType,
    },
    InvalidGame {
        expected_game_id: Option<GameId>,
        actual_game_id: Option<GameId>,
    },
    InvalidPlayer {
        expected_player: Option<ClientId>,
        actual_player: Option<ClientId>,
    },
}
