use crate::{ClientId, GameId, GameSettings};
use crate::game::tiles::tile::Grid;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct GameState {
    pub id: GameId,
    pub players: Vec<ClientId>,
    pub settings: GameSettings,
    pub map: Grid
}
