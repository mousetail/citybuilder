use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum CardinalDirection {
    North,
    East,
    South,
    West,
}

impl CardinalDirection {
    pub fn all() -> Vec<CardinalDirection> {
        return vec![
            CardinalDirection::North,
            CardinalDirection::East,
            CardinalDirection::South,
            CardinalDirection::West,
        ];
    }

    pub fn rotate_right_90(self) -> CardinalDirection {
        match self {
            CardinalDirection::North => CardinalDirection::East,
            CardinalDirection::East => CardinalDirection::South,
            CardinalDirection::South => CardinalDirection::West,
            CardinalDirection::West => CardinalDirection::North,
        }
    }

    pub fn rotate_left_90(self) -> CardinalDirection {
        match self {
            CardinalDirection::North => CardinalDirection::West,
            CardinalDirection::East => CardinalDirection::North,
            CardinalDirection::South => CardinalDirection::East,
            CardinalDirection::West => CardinalDirection::South,
        }
    }

    pub fn rotate_right_45(self) -> DiagonalDirection {
        match self {
            CardinalDirection::North => DiagonalDirection::NorthEast,
            CardinalDirection::East => DiagonalDirection::SouthEast,
            CardinalDirection::South => DiagonalDirection::SouthWest,
            CardinalDirection::West => DiagonalDirection::NorthWest,
        }
    }

    pub fn rotate_left_45(self) -> DiagonalDirection {
        match self {
            CardinalDirection::North => DiagonalDirection::NorthWest,
            CardinalDirection::East => DiagonalDirection::NorthEast,
            CardinalDirection::South => DiagonalDirection::SouthEast,
            CardinalDirection::West => DiagonalDirection::SouthWest,
        }
    }

    pub fn rotate_180(self) -> CardinalDirection {
        match self {
            CardinalDirection::North => CardinalDirection::South,
            CardinalDirection::East => CardinalDirection::West,
            CardinalDirection::South => CardinalDirection::North,
            CardinalDirection::West => CardinalDirection::East,
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum DiagonalDirection {
    NorthEast,
    SouthEast,
    SouthWest,
    NorthWest,
}

impl DiagonalDirection {
    pub fn all() -> Vec<DiagonalDirection> {
        return vec![
            DiagonalDirection::NorthEast,
            DiagonalDirection::SouthEast,
            DiagonalDirection::SouthWest,
            DiagonalDirection::NorthWest
        ];
    }

    pub fn rotate_right_90(self) -> DiagonalDirection {
        match self {
            DiagonalDirection::NorthWest => DiagonalDirection::NorthEast,
            DiagonalDirection::NorthEast => DiagonalDirection::SouthEast,
            DiagonalDirection::SouthEast => DiagonalDirection::SouthWest,
            DiagonalDirection::SouthWest => DiagonalDirection::NorthWest,
        }
    }

    pub fn rotate_left_90(self) -> DiagonalDirection {
        match self {
            DiagonalDirection::NorthWest => DiagonalDirection::SouthWest,
            DiagonalDirection::NorthEast => DiagonalDirection::NorthWest,
            DiagonalDirection::SouthEast => DiagonalDirection::NorthEast,
            DiagonalDirection::SouthWest => DiagonalDirection::NorthWest,
        }
    }

    pub fn rotate_right_45(self) -> CardinalDirection {
        match self {
            DiagonalDirection::NorthWest => CardinalDirection::North,
            DiagonalDirection::NorthEast => CardinalDirection::East,
            DiagonalDirection::SouthEast => CardinalDirection::South,
            DiagonalDirection::SouthWest => CardinalDirection::West,
        }
    }

    pub fn rotate_left_45(self) -> CardinalDirection {
        match self {
            DiagonalDirection::NorthWest => CardinalDirection::West,
            DiagonalDirection::NorthEast => CardinalDirection::North,
            DiagonalDirection::SouthEast => CardinalDirection::East,
            DiagonalDirection::SouthWest => CardinalDirection::North,
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum Direction {
    CardinalDirection(CardinalDirection),
    DiagonalDirection(DiagonalDirection),
}

impl Direction {
    pub fn rotate_right_90(self)->Direction {
        match self {
            Direction::CardinalDirection(dir)=>Direction::CardinalDirection(dir.rotate_right_90()),
            Direction::DiagonalDirection(dir)=>Direction::DiagonalDirection(dir.rotate_right_90())
        }
    }

    pub fn rotate_left_90(self)->Direction {
        match self {
            Direction::CardinalDirection(dir)=>Direction::CardinalDirection(dir.rotate_left_90()),
            Direction::DiagonalDirection(dir)=>Direction::DiagonalDirection(dir.rotate_left_90())
        }
    }

    pub fn rotate_right_45(self)->Direction {
        match self {
            Direction::CardinalDirection(dir)=>Direction::DiagonalDirection(dir.rotate_right_45()),
            Direction::DiagonalDirection(dir)=>Direction::CardinalDirection(dir.rotate_right_45())
        }
    }

    pub fn rotate_left_45(self)->Direction {
        match self {
            Direction::CardinalDirection(dir)=>Direction::DiagonalDirection(dir.rotate_left_45()),
            Direction::DiagonalDirection(dir)=>Direction::CardinalDirection(dir.rotate_left_45())
        }
    }
}
