use std::collections::HashMap;
use serde::{Serialize, Deserialize};
use crate::game::tiles::directions::CardinalDirection;

#[derive(Clone, Debug, Copy, Eq, PartialEq, Serialize, Deserialize)]
pub enum TileFeatureType {
    City,
    Road,
}

#[derive(Clone, Debug, Serialize, Deserialize, Eq, PartialEq)]
pub struct TileFeature {
    feature_type: TileFeatureType,
    edges: Vec<CardinalDirection>,
}

#[derive(Clone, Debug, Serialize, Deserialize, Eq, PartialEq)]
pub struct TileType {
    features: Vec<TileFeature>,
}

pub type Grid = HashMap<(i32, i32), TileType>;

enum TileCityShapeSize {
    InnerBox,
    OuterBox,
}

enum TileCityCurve {
    Full,
    BendIn,
    Straight,
    BendOut,
    Empty,
}

struct CityGlyph {
    orientation: CardinalDirection,
    position: (i32, i32),
    size: TileCityShapeSize,
    shape: TileCityCurve,
}

fn get_tile_glyphs(grid: Grid, position: (i32, i32)) -> Vec<CityGlyph> {
    let mut glyphs = vec![];

    for feature in grid[&position].features.iter() {

        for direction in CardinalDirection::all() {
            if feature.edges.contains(&direction) &&
                !feature.edges.contains(&direction.rotate_left_90()) &&
                !feature.edges.contains(&direction.rotate_right_90()) &&
                !feature.edges.contains(&direction.rotate_180()) {
                glyphs.push(CityGlyph {
                    orientation: direction,
                    position,
                    size: TileCityShapeSize::InnerBox,
                    shape: TileCityCurve::BendIn,
                })
            } else if feature.edges.contains(&direction) &&
                feature.edges.contains(&direction.rotate_left_90()) &&
                !feature.edges.contains(&direction.rotate_right_90()) &&
                !feature.edges.contains(&direction.rotate_180()) {
                glyphs.push(CityGlyph {
                    orientation: direction,
                    position,
                    size: TileCityShapeSize::InnerBox,
                    shape: TileCityCurve::Straight,
                })
            } else if !feature.edges.contains(&direction) &&
                !feature.edges.contains(&direction.rotate_left_90()) &&
                !feature.edges.contains(&direction.rotate_right_90()) {
                glyphs.push(CityGlyph {
                    orientation: direction,
                    position,
                    size: TileCityShapeSize::InnerBox,
                    shape: TileCityCurve::Empty,
                })
            } else if feature.edges.contains(&direction) &&
                feature.edges.contains(&direction.rotate_left_90()) &&
                feature.edges.contains(&direction.rotate_right_90()) {
                glyphs.push(CityGlyph {
                    orientation: direction,
                    position,
                    size: TileCityShapeSize::InnerBox,
                    shape: TileCityCurve::Full,
                })
            } else if !feature.edges.contains(&direction) &&
                feature.edges.contains(&direction.rotate_left_90()) &&
                feature.edges.contains(&direction.rotate_right_90()) {
                glyphs.push(
                    CityGlyph {
                        orientation: direction,
                        position,
                        size: TileCityShapeSize::InnerBox,
                        shape: TileCityCurve::BendOut,
                    }
                )
            }
        }
    };

    return glyphs;
}
