use wasm_bindgen::prelude::*;

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just
    // `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    pub fn log(s: &str);

    #[wasm_bindgen(js_namespace = console)]
    pub fn error(s: &str);
}

#[cfg(target_arch = "wasm32")]
pub fn log_string(s: &str) {
    log(s);
}

#[cfg(target_arch = "wasm32")]
pub fn log_string_error(s: &str) {
    error(s);
}

#[cfg(not(target_arch = "wasm32"))]
pub fn log_string(s: &str) {
    println!("{}", s)
}

#[cfg(not(target_arch = "wasm32"))]
pub fn log_string_error(s: &str) {
    eprintln!("{}", s)
}

#[macro_export]
macro_rules! console_log {
    ($($t:tt)*) => ($crate::console::log_string(&format_args!($($t)*).to_string()))
}

#[macro_export]
macro_rules! console_error {
    ($($t:tt)*) => ($crate::console::log_string_error(&format_args!($($t)*).to_string()))
}
