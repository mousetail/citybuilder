use yew_router::prelude::*;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/lobby")]
    Lobby,
    #[at("/lobby/:game_id")]
    LobbyGame { game_id: String },
    #[at("/game/:game_id")]
    Game { game_id: String },
}
