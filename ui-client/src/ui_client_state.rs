use shared::client_location::ClientStateType;
use shared::game::game_state::GameState;
use shared::lobby::lobby_state::GameLobbyState;
use shared::lobby_state::LobbyState;
use shared::versioned_cell::VersionedCell;

#[derive(PartialEq, Clone)]
pub enum UiClientState {
    None,
    InLobby(VersionedCell<LobbyState>),
    InGameLobby(VersionedCell<GameLobbyState>),
    InGame(VersionedCell<GameState>),
}

impl Default for UiClientState {
    fn default() -> Self {
        UiClientState::None
    }
}

impl UiClientState {
    pub fn get_client_state_type(&self) -> ClientStateType {
        match self {
            UiClientState::None => ClientStateType::None,
            UiClientState::InLobby(_) => ClientStateType::InLobby,
            UiClientState::InGameLobby(_) => ClientStateType::InGameLobby,
            UiClientState::InGame(_) => ClientStateType::InGame,
        }
    }
}
