use std::borrow::{Borrow, BorrowMut};
use wasm_bindgen::closure::Closure;
use web_sys::{HtmlCanvasElement, window};
use yew::prelude::*;
use crate::canvas_drawing::CanvasDrawing;
use wasm_bindgen::JsCast;

#[derive(Properties, PartialEq)]
pub struct GameMainComponentProps {

}

pub struct GameMainComponent {
    canvas: NodeRef,
    renderer: Option<CanvasDrawing>,
    request_animation_frame_callback: Option<Closure<dyn Fn() -> ()>>
}

pub enum GameMainMessage {
    RenderCanvas
}

impl Component for GameMainComponent {
    type Message = GameMainMessage;
    type Properties = GameMainComponentProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            renderer: None,
            canvas: NodeRef::default(),
            request_animation_frame_callback: None
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            GameMainMessage::RenderCanvas => {
                if let Some(renderer) = self.renderer.borrow_mut() {
                    renderer.draw();
                }

                if let Some(callback) = self.request_animation_frame_callback.borrow() {
                    window().unwrap().request_animation_frame(callback.as_ref().unchecked_ref()).unwrap();
                }
                false
            }
        }
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html!(
            <canvas ref={self.canvas.clone()}></canvas>
        )
    }

    fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
        if first_render {
            let canvas: HtmlCanvasElement = self.canvas.get().unwrap().dyn_into().unwrap();

            let drawing = CanvasDrawing::new(
                canvas.get_context("2d").unwrap().unwrap().dyn_into().unwrap()
            );

            self.renderer = Some(
                drawing
            );

            let link = ctx.link();
            let callback = link.callback(|_| GameMainMessage::RenderCanvas);
            let call_callback = move || callback.emit(());
            let callback_wrap = Box::new(call_callback) as Box<dyn Fn() ->()>;
            let callback_closure = Closure::wrap(callback_wrap);

            window().unwrap().request_animation_frame(callback_closure.as_ref().unchecked_ref()).unwrap();
            self.request_animation_frame_callback = Some(callback_closure);
        }
    }
}
