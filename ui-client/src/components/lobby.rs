use crate::{console_log, Route, SerializingSocket, UiClientState};
use shared::events::client_event::{ClientEvent, LobbyEvent};
use shared::lobby_state::LobbyState;
use shared::versioned_cell::VersionedCell;
use std::borrow::Borrow;
use yew::prelude::*;
use yew_router::prelude::*;

pub struct Lobby {}

#[derive(PartialEq, Properties)]
pub struct LobbyProps {
    pub socket: SerializingSocket,
    pub state: UiClientState,
}

impl Lobby {
    fn render_when_data_available(&self, state: &LobbyState, ctx: &Context<Self>) -> Html {
        let games = state
            .games
            .values()
            .map(|game| {
                html! {
                    <Link<Route> classes={classes!("room-list-room")} to={Route::LobbyGame { game_id: game.id.to_string() }}>
                        <div>
                            {format!("{:}", game.id)}
                        </div>
                        <div>
                            {format!("{:?}", game.settings.speed)}
                        </div>
                        <div>
                            {format!("{:?}", game.settings.mode)}
                        </div>
                    </Link<Route>>
                }
            })
            .collect::<Html>();

        let socket = ctx.props().socket.borrow();
        let create_game_button_callback = {
            let socket = socket.clone();
            move |_ev| {
                socket
                    .send_event(ClientEvent::LobbyEvent(LobbyEvent::CreateRoom))
                    .unwrap();
            }
        };

        html! {
                <>
                <h1>
                    { "Lobby" }
                </h1>
                <div class={classes!("room-list")}>
                    <div class={classes!("room-list-room")}>
                        <div>
                            {"ID"}
                        </div>
                        <div>
                            {"Speed"}
                        </div>
                        <div>
                            {"Mode"}
                        </div>
                    </div>
                {
                    games
                }
                </div>
                <button onclick={create_game_button_callback}>
                    { "Create Game" }
                </button>
                </>
        }
    }
}

impl Component for Lobby {
    type Message = ();
    type Properties = LobbyProps;

    fn create(ctx: &Context<Self>) -> Self {
        ctx.props()
            .socket
            .send_event(ClientEvent::EnterLobby)
            .expect("Failed to connect to socket server");

        Lobby {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        console_log!("Updating");

        let result: Html = if let UiClientState::InLobby(state) = ctx.props().state.borrow() {
            self.render_when_data_available(&VersionedCell::borrow(state), ctx)
        } else {
            let result = html! {
                <div> {"loading2" } </div>
            };

            result
        };
        return result;
    }
}
