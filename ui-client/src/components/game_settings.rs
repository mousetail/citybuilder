use crate::{console_log, SerializingSocket, UiClientState};
use shared::events::client_event::{ClientEvent, LobbyEvent};
use shared::game_settings::{GameId, GameSettings, GameSpeed};
use shared::versioned_cell::VersionedCell;
use std::borrow::Borrow;
use yew::prelude::*;
use shared::events::game_events::GameEvent;
use shared::lobby::lobby_state::GameLobbyState;

pub struct GameSettingsComponent {}

#[derive(PartialEq, Properties)]
pub struct GameSettingsProps {
    pub socket: SerializingSocket,
    pub state: UiClientState,
    pub game_id: GameId,
}

impl GameSettingsComponent {
    fn render_when_data_available(&self, state: &GameLobbyState, ctx: &Context<Self>) -> Html {
        let props = ctx.props();
        let socket = props.socket.clone();
        let game_id = props.game_id;
        let game_settings = state.settings.clone();
        let on_increase_speed = move |_ev: MouseEvent| {
            socket
                .send_event(ClientEvent::LobbyEvent(LobbyEvent::UpdateGameSettings(
                    game_id,
                    GameSettings {
                        speed: GameSpeed::Fast,
                        ..game_settings
                    },
                )))
                .unwrap();
        };
        let socket = props.socket.clone();
        let on_decrease_speed = move |_ev: MouseEvent| {
            socket
                .send_event(ClientEvent::LobbyEvent(LobbyEvent::UpdateGameSettings(
                    game_id,
                    GameSettings {
                        speed: GameSpeed::Slow,
                        ..game_settings
                    },
                )))
                .unwrap();
        };

        let socket = props.socket.clone();
        let on_start_game = move |_ev: MouseEvent| {
            socket.send_event(ClientEvent::GameEvent(GameEvent::StartGame{game_id})).unwrap()
        };

        html! {
                <>
                <h1>
                    { "Game Settings" }
                </h1>
                <h2>{"Game ID: \""} {game_id} {"\""}</h2>
                <div>
                    <button onclick={on_decrease_speed}>{ " Down" }</button>
                    { "Speed: " }
                    { format!("{:?}", state.settings.speed) }
                    <button onclick={on_increase_speed}>{ " Up" }</button>
                </div>

                <div>
                    <button>{ " Down" }</button>
                    { "Mode: " }
                    { format!("{:?}", state.settings.mode) }
                    <button>{ " Up" }</button>
                </div>

                <button onclick={on_start_game} >{ "Start Game "}</button>
                </>
        }
    }
}

impl Component for GameSettingsComponent {
    type Message = ();
    type Properties = GameSettingsProps;

    fn create(ctx: &Context<Self>) -> Self {
        let props = ctx.props();

        props
            .socket
            .send_event(ClientEvent::EnterGameLobby(props.game_id))
            .expect("Failed to connect to socket server");

        GameSettingsComponent {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let result: Html = if let UiClientState::InGameLobby(state) = ctx.props().state.borrow() {
            self.render_when_data_available(&VersionedCell::borrow(state), ctx)
        } else {
            let result = html! {
                <div> {"loading" } </div>
            };

            result
        };
        return result;
    }
}
