mod components;
mod console;
mod routes;
mod socket;
mod ui_client_state;
mod canvas_drawing;

use crate::components::game_settings::GameSettingsComponent;
use crate::components::lobby::Lobby;
use crate::routes::Route;
use crate::socket::decoded_sockets::{MutationMessageEvent, SerializingSocket};
use crate::ui_client_state::UiClientState;
use crate::Msg::HandleMutations;
use shared::client_location::{ClientLocation, ClientStateType};
use shared::game_settings::GameId;
use shared::lobby_state::LobbyState;
use shared::mutation_error::MutationError;
use shared::mutations::lobby_mutations::apply_lobby_mutation;
use shared::versioned_cell::VersionedCell;
use shared::{LobbyMutation, LobbyMutationType, Mutation};
use std::borrow::BorrowMut;
use std::ops::DerefMut;
use yew::prelude::*;
use yew_router::prelude::*;
use shared::lobby::lobby_state::GameLobbyState;
use crate::components::game_component::GameMainComponent;

enum Msg {
    HandleMutations(Vec<Mutation>),
    Connected,
    Disconnected,
}

enum ConnectedState {
    NeverConnected,
    Connected,
    Disconnected,
}

struct Model {
    state: UiClientState,
    socket: SerializingSocket,
    connected: ConnectedState,
}

fn switch(socket: SerializingSocket, state: UiClientState) -> impl Fn(&Route) -> Html {
    move |routers: &Route| match routers {
        Route::Home => html! {
            <div>
                <h1>{"Home"}</h1>
                <Link<Route> to={Route::Lobby}>{"Lobby"}</Link<Route>>
            </div>
        },
        Route::Lobby => html! {
            <Lobby socket={socket.clone()} state={state.clone()}/>
        },
        Route::LobbyGame { game_id } => html! {
            <GameSettingsComponent socket={socket.clone()} state={state.clone()} game_id={GameId::from_string(game_id)}/>
        },
        Route::Game {game_id} => html! { <GameMainComponent/> },
        _ => html! {
            <h1>{ "Page" }</h1>
        },
    }
}

impl Model {
    fn on_socket_event(event: MutationMessageEvent) -> Option<Msg> {
        match event {
            MutationMessageEvent::Query(msg) => Some(HandleMutations(msg)),
            MutationMessageEvent::Connected => Some(Msg::Connected),
            MutationMessageEvent::Disconnected => None,
            MutationMessageEvent::Error(_) => None,
        }
    }

    fn handle_lobby_mutation(&mut self, mutation: &LobbyMutation) -> Result<bool, MutationError> {
        match self.state.borrow_mut() {
            UiClientState::InLobby(lobby_state) => {
                let mut lobby_state = VersionedCell::borrow_mut(lobby_state);

                apply_lobby_mutation(lobby_state.deref_mut(), mutation);
                console_log!("Games: {:?}", lobby_state.games);
                return Ok(true);
            }
            UiClientState::InGameLobby(lobby_state) => {
                let mut lobby_state = VersionedCell::borrow_mut(lobby_state);
                if let LobbyMutationType::UpdateRoomSettings(setting) = &mutation.mutation_type {
                    if mutation.game_id != lobby_state.id {
                        return Err(MutationError::InvalidGame {
                            expected_game_id: Some(lobby_state.id),
                            actual_game_id: Some(mutation.game_id),
                        });
                    }

                    lobby_state.settings = setting.clone();

                    return Ok(true);
                } else {
                    return Result::Err(MutationError::InvalidPage {
                        expected_page: ClientStateType::InLobby,
                        actual_page: ClientStateType::InGameLobby,
                    });
                }
            }
            _ => {
                return Result::Err(MutationError::InvalidPage {
                    expected_page: ClientStateType::InLobby,
                    actual_page: self.state.get_client_state_type(),
                })
            }
        };
    }

    fn on_mutation(&mut self, mutation: &Mutation) -> Result<bool, MutationError> {
        console_log!("Got mutation: {:?}", mutation);
        match mutation {
            Mutation::MoveClient {
                client: _client,
                destination,
            } => match destination {
                ClientLocation::None => {}
                ClientLocation::InGame(_) => {}
                ClientLocation::InGameLobby(game_id) => {
                    self.state = UiClientState::InGameLobby(VersionedCell::new(
                        GameLobbyState::new_with_id(*game_id),
                    ));
                    return Ok(true);
                }
                ClientLocation::InLobby => {
                    self.state = UiClientState::InLobby(VersionedCell::new(LobbyState::default()));
                    return Ok(true);
                }
            },
            Mutation::LobbyMutation(mutation) => return self.handle_lobby_mutation(mutation),
            Mutation::GameMutation { .. } => {}
            Mutation::ChatMessage(_) => {}
            Mutation::ClientError { .. } => {}
        };
        return Ok(false);
    }
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let link = ctx.link();

        let callback = link.batch_callback(Model::on_socket_event);
        let socket_info = SerializingSocket::new(move |ev| callback.emit(ev));

        Self {
            state: UiClientState::default(),
            socket: socket_info,
            connected: ConnectedState::NeverConnected,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        let mut should_update = false;
        match msg {
            HandleMutations(mutations) => {
                for mutation in mutations {
                    should_update |= self.on_mutation(&mutation).unwrap_or_else(|err| {
                        console_error!("Error updating: {:?}", err);
                        true
                    });
                }
            }
            Msg::Connected => {
                self.connected = ConnectedState::Connected;
                should_update = true
            }
            Msg::Disconnected => self.connected = ConnectedState::Disconnected,
        }
        should_update
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        if let ConnectedState::NeverConnected = self.connected {
            return html! {
                <h2>{ "Connecting..." }</h2>
            };
        }

        // This gives us a component's "`Scope`" which allows us to send messages, etc to the component.
        html! {
            <BrowserRouter>
                <Switch<Route> render={Switch::render(switch(self.socket.clone(), self.state.clone()))}/>
            </BrowserRouter>
        }
    }
}

fn main() {
    #[cfg(target_arch = "wasm32")]
    console_error_panic_hook::set_once();
    yew::start_app::<Model>();
}
