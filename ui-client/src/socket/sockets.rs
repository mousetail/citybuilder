use wasm_bindgen::convert::FromWasmAbi;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{ErrorEvent, Event, MessageEvent, WebSocket};
use yew::Callback;

pub enum SocketEvent {
    TextMessage(String),
    BinaryMessage(Vec<u8>),
    Connected,
    Disconnected,
    Error(String),
}

#[derive(Clone, PartialEq, Eq)]
pub struct SocketInfo {
    socket: WebSocket,
}

impl SocketInfo {
    pub fn send_string(&self, message: &str) -> Result<(), JsValue> {
        self.socket.send_with_str(message)
    }

    pub fn send_binary(&self, data: &[u8]) -> Result<(), JsValue> {
        self.socket.send_with_u8_array(data)
    }
}

fn create_event_listener<T, E>(callback: E) -> js_sys::Function
where
    T: 'static + FromWasmAbi,
    E: Fn(T) -> () + 'static,
    // F: FnMut(T) -> () + WasmClosure + 'static
{
    let on_open_closure = Closure::wrap(Box::new(callback) as Box<dyn FnMut(T) -> ()>);
    return on_open_closure.into_js_value().unchecked_into();
}

pub fn start_websocket(
    on_event: impl Fn(SocketEvent) -> () + Clone + 'static,
) -> Result<SocketInfo, JsValue> {
    // Connect to an echo server
    let ws = WebSocket::new("ws://localhost:8081")?;
    // For small binary messages, like CBOR, Arraybuffer is more efficient than Blob handling
    ws.set_binary_type(web_sys::BinaryType::Arraybuffer);
    // create callback

    {
        let on_event = on_event.clone();
        let on_message = create_event_listener(move |ev: MessageEvent| {
            if let Ok(buffer) = ev.data().dyn_into::<js_sys::ArrayBuffer>() {
                let array = js_sys::Uint8Array::new(&buffer);
                on_event(SocketEvent::BinaryMessage(array.to_vec()));
            } else if let Ok(_) = ev.data().dyn_into::<web_sys::Blob>() {
                panic!("Expected array buffer, not blob")
            } else if let Ok(buffer) = ev.data().dyn_into::<js_sys::JsString>() {
                on_event(SocketEvent::TextMessage(buffer.as_string().unwrap()))
            } else {
                panic!("Unexpected JS value {:?}", ev)
            }
        });
        ws.set_onmessage(Some(&on_message));
    }

    {
        let on_event = on_event.clone();
        let on_error = create_event_listener(move |ev: ErrorEvent| {
            on_event(SocketEvent::Error(format!("{:?}", ev.to_string())))
        });
        ws.set_onerror(Some(&on_error));
    }

    {
        let on_event = on_event.clone();
        let on_open = create_event_listener(move |_ev: Event| on_event(SocketEvent::Connected));
        ws.set_onopen(Some(&on_open));
    }

    {
        let on_event = on_event.clone();
        let on_close = create_event_listener(move |_ev: Event| on_event(SocketEvent::Disconnected));
        ws.set_onclose(Some(&on_close));
    }

    Ok(SocketInfo { socket: ws })
}
