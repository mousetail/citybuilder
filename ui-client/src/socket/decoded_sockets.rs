use crate::socket::sockets::{start_websocket, SocketEvent, SocketInfo};
use shared::events::client_event::ClientEvent;
use shared::Mutation;
use wasm_bindgen::JsValue;

#[derive(Clone, Eq, PartialEq)]
pub struct SerializingSocket {
    socket: SocketInfo,
}

pub enum MutationMessageEvent {
    Query(Vec<Mutation>),
    Connected,
    Disconnected,
    Error(String),
}

#[derive(Debug)]
pub enum SendMessageError {
    SerializationError(serde_json::Error),
    SocketError(JsValue),
}

impl From<serde_json::Error> for SendMessageError {
    fn from(val: serde_json::Error) -> Self {
        SendMessageError::SerializationError(val)
    }
}

impl From<JsValue> for SendMessageError {
    fn from(val: JsValue) -> Self {
        SendMessageError::SocketError(val)
    }
}

impl SerializingSocket {
    pub fn new(on_event_callback: impl Fn(MutationMessageEvent) -> () + 'static + Clone) -> Self {
        let socket = start_websocket(move |ev| {
            on_event_callback(match ev {
                SocketEvent::TextMessage(text) => {
                    MutationMessageEvent::Query(serde_json::from_str(&text).unwrap())
                }
                SocketEvent::BinaryMessage(_) => {
                    panic!("Expected a text message")
                }
                SocketEvent::Connected => MutationMessageEvent::Connected,
                SocketEvent::Disconnected => MutationMessageEvent::Disconnected,
                SocketEvent::Error(msg) => MutationMessageEvent::Error(msg),
            })
        })
        .unwrap();

        Self { socket }
    }

    pub fn send_event(&self, ev: ClientEvent) -> Result<(), SendMessageError> {
        self.socket.send_string(&serde_json::to_string(&ev)?)?;
        Ok(())
    }
}
