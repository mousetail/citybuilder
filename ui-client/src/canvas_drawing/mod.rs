use wasm_bindgen::closure::Closure;
use web_sys::{CanvasRenderingContext2d, Document, HtmlImageElement, Window};
use wasm_bindgen::JsCast;
use yew::Callback::Callback;

pub struct CanvasDrawing {
    position: (f64, f64),
    zoom: f32,
    context: CanvasRenderingContext2d,
    tile_map: HtmlImageElement
}

impl CanvasDrawing {
    pub fn new(context: CanvasRenderingContext2d) -> Self {
        let position = (0.,0.);
        let zoom = 1.0;
        let window = web_sys::window().unwrap();
        let document = window.document().unwrap();
        let tile_map = document.create_element("img").unwrap().dyn_into::<HtmlImageElement>().unwrap();
        tile_map.set_src("https://upload.wikimedia.org/wikipedia/commons/e/ee/Solar_eclips_1999_6.jpg");

        Self {
            position,
            zoom,
            context,
            tile_map
        }
    }

    pub fn draw(&mut self) {
        self.position.0 += 1.;

        self.context.draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
            &self.tile_map,
            0., 0.,
            64., 64.,
            self.position.0, self.position.1,
            64., 64.
        ).unwrap();

        // let callback = Closure::wrap(Box::new(
        //         move || self.draw()
        //     )
        //     as Box<dyn Fn() -> ()>
        // );
        //
        // web_sys::window().unwrap().request_animation_frame(callback.as_ref().unchecked_ref());
    }
}
