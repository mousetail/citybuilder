use shared::client_state::{ClientId, ClientState};
use shared::game_settings::GameId;
use shared::lobby_state::LobbyState;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use shared::game::game_state::GameState;

#[derive(Default)]
pub struct ServerState {
    pub active_games: HashMap<GameId, GameState>,
    pub lobby_state: LobbyState,
    pub clients: HashMap<ClientId, ClientState>,
}

impl ServerState {
    pub fn new() -> ServerState {
        Default::default()
    }

    pub fn insert_player(&mut self, client: ClientState) {
        self.clients.insert(client.id, client);
    }

    pub fn remove_player_by_id(&mut self, player_id: ClientId) {
        self.clients.remove(&player_id).unwrap();
    }
}

pub type ServerStateRef = Arc<Mutex<ServerState>>;
