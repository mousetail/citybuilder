mod chat;
mod client_mutation_filter;
mod events;
mod lobby;
pub mod mutations;
mod server_state;
mod game;

use std::borrow::BorrowMut;
use std::time::Instant;
use std::{
    collections::HashMap,
    env,
    io::Error as IoError,
    net::SocketAddr,
    sync::{Arc, Mutex},
};

use futures_channel::mpsc::{unbounded, UnboundedSender};
use futures_util::{future, pin_mut, stream::TryStreamExt, StreamExt};

use crate::client_mutation_filter::filter_mutation;
use crate::events::{handle_event, ServerEvent};
use mutations::apply_mutation;
use server_state::{ServerState, ServerStateRef};
use shared::events::client_event::ClientEvent;
use shared::client_location::ClientLocation;
use shared::client_state::{ClientId, ClientState};
use tokio::net::{TcpListener, TcpStream};
use tokio_tungstenite::tungstenite::Message;

type Tx = UnboundedSender<Message>;

fn handle_client_event(
    event: ServerEvent,
    server_state: &ServerStateRef,
    sockets: &Arc<Mutex<HashMap<ClientId, Tx>>>,
    client: ClientId,
) {
    let mut server_state = server_state.lock().unwrap();

    let response = handle_event(event, &server_state, client);

    for mutation in &response {
        apply_mutation(mutation, server_state.borrow_mut()).unwrap_or_else(|e| {
            eprintln!("Failed to handle mutation {:?} because {:?}", mutation, e);
        })
    }

    if response.len() == 0 {
        return;
    }

    let sockets = sockets.lock().unwrap();
    for mutation in &response {
        filter_mutation(
            mutation,
            &server_state.borrow_mut(),
            |player_id, mutation| {
                if let Some(channel) = sockets.get(&player_id) {
                    channel
                        .unbounded_send(Message::Text(
                            serde_json::to_string(&vec![mutation]).unwrap(),
                        ))
                        .unwrap_or_else(|err| {
                            eprintln!("Error sending mutation: {:?}", err);
                        });
                }
            },
        );
    }
    // let response_serialized =
    //     serde_json::to_string(&response).expect("Failed to serialize value");
    //
    // let sockets = sockets.lock().unwrap();
    // for player in &server_state.lobby_state.players {
    //     if let Some(channel) = sockets.get(&player) {
    //         channel
    //             .unbounded_send(Message::Text(response_serialized.clone()))
    //             .unwrap()
    //     };
    // }
}

async fn handle_connection(
    server_state: ServerStateRef,
    sockets: Arc<Mutex<HashMap<ClientId, Tx>>>,
    raw_stream: TcpStream,
    addr: SocketAddr,
) {
    let client_id = ClientId::new();
    println!("Incoming TCP connection from: {}", addr);

    let ws_stream = tokio_tungstenite::accept_async(raw_stream)
        .await
        .expect("Error during the websocket handshake occurred");

    println!("WebSocket connection established: {}", addr);

    // Insert the write part of this peer to the peer map.
    let (tx, rx) = unbounded();

    server_state.lock().unwrap().insert_player(ClientState {
        id: client_id,
        location: ClientLocation::None,
    });

    sockets.lock().unwrap().insert(client_id, tx);

    let (outgoing, incoming) = ws_stream.split();

    let broadcast_incoming = incoming.try_for_each(|msg| {
        let start_time = Instant::now();

        println!(
            "Received a message from {}: {}",
            addr,
            msg.to_text().unwrap()
        );

        if msg.is_close() || msg.is_ping() || msg.is_pong() {
            return future::ok(());
        }

        let msg_decoded = serde_json::from_str::<ClientEvent>(msg.to_text().unwrap());

        match msg_decoded {
            Err(e) => {
                eprintln!("Got invalid message: {}", e)
            }
            Ok(event) => {
                handle_client_event(
                    ServerEvent::ClientEvent {
                        event,
                        client: client_id,
                    },
                    &server_state,
                    &sockets,
                    client_id,
                );
            }
        }

        let end_time = Instant::now();
        println!("Time to handle request: {:?}", end_time - start_time);

        future::ok(())
    });

    let receive_from_others = rx.map(Ok).forward(outgoing);

    handle_client_event(
        ServerEvent::ClientConnected(client_id),
        &server_state,
        &sockets,
        client_id,
    );

    pin_mut!(broadcast_incoming, receive_from_others);
    future::select(broadcast_incoming, receive_from_others).await;

    handle_client_event(
        ServerEvent::ClientDisconnected(client_id),
        &server_state,
        &sockets,
        client_id,
    );
    println!("{} disconnected", &addr);
    {
        let mut state = server_state.lock().unwrap();
        state.remove_player_by_id(client_id);
        // state.broadcast(format!("Player {} disconnected", &addr)).unwrap();
    }
}

#[tokio::main]
async fn main() -> Result<(), IoError> {
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:8081".to_string());

    let state = ServerStateRef::new(Mutex::new(ServerState::new()));
    let sockets = Arc::new(Mutex::new(HashMap::new()));

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let listener = try_socket.expect("Failed to bind");
    println!("Listening on: {}", addr);

    // Let's spawn the handling of each connection in a separate task.
    while let Ok((stream, addr)) = listener.accept().await {
        tokio::spawn(handle_connection(
            state.clone(),
            sockets.clone(),
            stream,
            addr,
        ));
    }

    Ok(())
}
