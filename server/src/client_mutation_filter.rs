use crate::ServerState;
use shared::client_location::ClientLocation;
use shared::client_state::ClientId;
use shared::game::game_state::GameState;
use shared::Mutation::ClientError;
use shared::{LobbyMutation, LobbyMutationType, Mutation};

fn move_client(
    destination: &ClientLocation,
    client: &ClientId,
    game_state: &ServerState,
    callback: impl Fn(ClientId, &Mutation),
) {
    callback(
        *client,
        &Mutation::MoveClient {
            client: *client,
            destination: *destination,
        },
    );

    match destination {
        ClientLocation::None => (),
        ClientLocation::InGame(_game_id) => (),
        ClientLocation::InGameLobby(game_id) => {
            let game_settings = game_state.lobby_state.games.get(game_id).unwrap();
            callback(
                *client,
                &Mutation::LobbyMutation(LobbyMutation {
                    game_id: *game_id,
                    mutation_type: LobbyMutationType::UpdateRoomSettings(
                        game_settings.settings.clone(),
                    ),
                }),
            );
        }
        ClientLocation::InLobby => {
            for game_state in game_state.lobby_state.games.values() {
                callback(
                    *client,
                    &Mutation::LobbyMutation(LobbyMutation{
                        game_id: game_state.id,
                        mutation_type: LobbyMutationType::AddRoom(
                            game_state.clone(),
                        )
                    }),
                )
            }
        }
    }
}

pub fn filter_mutation(
    mutation: &Mutation,
    server_state: &ServerState,
    callback: impl Fn(ClientId, &Mutation) -> (),
) {
    match mutation {
        Mutation::LobbyMutation(lobby_mutation) => {
            for player in server_state.lobby_state.players.iter() {
                callback(*player, &mutation);
            }

            for player in server_state.lobby_state.games.get(&lobby_mutation.game_id).unwrap().players.iter() {
                callback(*player, &mutation);
            }
        }
        Mutation::GameMutation { game_id, .. } => {
            for player in &server_state.active_games.get(&game_id).unwrap().players {
                callback(player.id, &mutation)
            }
        }
        Mutation::ChatMessage(_) => {
            for player in server_state.clients.values() {
                callback(player.id, &mutation)
            }
        }
        Mutation::MoveClient {
            client,
            destination,
        } => {
            move_client(destination, client, server_state, callback);
        }
        Mutation::ClientError {
            client,
            message: _message,
        } => (callback(*client, mutation)),
    }
}
