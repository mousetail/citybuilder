use shared::game_settings::{GameId, GameSettings};
use shared::lobby_state::LobbyState;
use shared::{LobbyMutation, LobbyMutationType, Mutation};
use shared::lobby::lobby_state::GameLobbyState;

pub fn create_room() -> Vec<Mutation> {
    let game_id = GameId::new();

    return vec![Mutation::LobbyMutation(LobbyMutation{
        game_id,
        mutation_type: LobbyMutationType::AddRoom (
            GameLobbyState {
                id: game_id,
                settings: GameSettings::default(),
                players: vec![],
            },
        ),
    })];
}
