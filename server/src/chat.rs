use shared::Mutation;

pub(crate) fn send_chat_message(msg: String) -> Vec<Mutation> {
    return vec![Mutation::ChatMessage(msg)];
}
