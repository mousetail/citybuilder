use shared::client_state::ClientState;
use shared::game::game_state::GameState;
use shared::game::tiles::tile::Grid;
use shared::game_settings::{GameId, GameSettings};
use shared::lobby::lobby_state::GameLobbyState;

fn init_game(id: GameId, lobby_state: GameLobbyState)-> GameState {
    return GameState {
        id,
        players: lobby_state.players,
        settings: lobby_state.settings,
        map: Grid::new()
    }
}
