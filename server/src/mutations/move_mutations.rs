use crate::ServerState;
use shared::client_location::ClientLocation;
use shared::client_state::{ClientId, ClientState};
use shared::game::game_state::GameState;
use shared::mutation_error::MutationError;
use std::ops::Index;

pub(super) fn apply_client_move_mutation(
    server_state: &mut ServerState,
    client: ClientId,
    destination: &ClientLocation,
) -> Result<(), MutationError> {
    let client_current_location = server_state
        .clients
        .get(&client)
        .ok_or(MutationError::InvalidPlayer {
            expected_player: None,
            actual_player: Some(client),
        })?
        .location;
    remove_client_from_old_location(server_state, client, client_current_location)?;
    add_client_to_new_location(server_state, client, destination)?;

    server_state.clients.get_mut(&client).unwrap().location = *destination;
    Ok(())
}

fn add_client_to_new_location(
    server_state: &mut ServerState,
    client: ClientId,
    destination: &ClientLocation,
) -> Result<(), MutationError> {
    match destination {
        ClientLocation::InGame(_) => {
            todo!()
        }
        ClientLocation::InGameLobby(game_id) => server_state
            .lobby_state
            .games
            .get_mut(&game_id)
            .ok_or(MutationError::InvalidGame {
                actual_game_id: Some(*game_id),
                expected_game_id: None,
            })?
            .players
            .push(client),
        ClientLocation::None => (),
        ClientLocation::InLobby => server_state.lobby_state.players.push(client),
    }

    Ok(())
}

fn remove_client_from_old_location(
    server_state: &mut ServerState,
    client: ClientId,
    old_location: ClientLocation,
) -> Result<(), MutationError> {
    match old_location {
        ClientLocation::None => (),
        ClientLocation::InGame(_) => (),
        ClientLocation::InGameLobby(game_id) => {
            if let Some(game) = server_state.lobby_state.games.get_mut(&game_id) {
                if let Some(index) = game.players.iter().position(|&c| c == client) {
                    game.players.swap_remove(index);
                }
            }
        }
        ClientLocation::InLobby => {
            if let Some(index) = server_state
                .lobby_state
                .players
                .iter()
                .position(|&c| c == client)
            {
                server_state.lobby_state.players.swap_remove(index);
            }
        }
    }

    Ok(())
}
