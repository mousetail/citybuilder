mod move_mutations;

use crate::mutations::move_mutations::apply_client_move_mutation;
use crate::ServerState;
use shared::mutation_error::MutationError;
use shared::mutations::lobby_mutations::apply_lobby_mutation;
use shared::Mutation;

pub fn apply_mutation(
    mutation_type: &Mutation,
    state: &mut ServerState,
) -> Result<(), MutationError> {
    match mutation_type {
        Mutation::LobbyMutation(mutation) => {
            apply_lobby_mutation(&mut state.lobby_state, mutation)?
        }
        Mutation::GameMutation { .. } => {}
        Mutation::ChatMessage(_) => {}
        Mutation::MoveClient {
            client,
            destination,
        } => apply_client_move_mutation(state, *client, destination)?,
        Mutation::ClientError { .. } => (),
    }
    Ok(())
}
