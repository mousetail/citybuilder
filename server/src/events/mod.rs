use crate::chat::send_chat_message;
use crate::events::client_events::handle_client_event;
use crate::events::lobby_events::handle_lobby_event;
use crate::ServerState;
use shared::events::client_event::ClientEvent;
use shared::client_location::ClientLocation;
use shared::client_state::ClientId;
use shared::Mutation;
use shared::Mutation::MoveClient;

mod client_events;
pub mod lobby_events;
pub mod game_events;

pub enum ServerEvent {
    ClientEvent {
        client: ClientId,
        event: ClientEvent,
    },
    ClientConnected(ClientId),
    ClientDisconnected(ClientId),
}

pub fn handle_event(
    event: ServerEvent,
    server_state: &ServerState,
    client: ClientId,
) -> Vec<Mutation> {
    match event {
        ServerEvent::ClientEvent { client, event } => {
            handle_client_event(event, client, server_state)
        }
        ServerEvent::ClientConnected(_) => {
            vec![]
        }
        ServerEvent::ClientDisconnected(_) => {
            println!("Handle disconnection event");
            vec![MoveClient {
                client,
                destination: ClientLocation::None,
            }]
        }
    }
}
