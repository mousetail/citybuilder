use crate::lobby::create_room::create_room;
use crate::ServerState;
use shared::events::client_event::LobbyEvent;
use shared::client_location::ClientLocation;
use shared::client_state::ClientId;
use shared::game_settings::{GameId, GameSettings};
use shared::lobby_state::LobbyState;
use shared::{LobbyMutation, LobbyMutationType, Mutation};

fn player_join_room(game_id: GameId, client: ClientId, lobby_state: &LobbyState) -> Vec<Mutation> {
    if lobby_state.games.contains_key(&game_id) {
        vec![Mutation::MoveClient {
            client,
            destination: ClientLocation::InGameLobby(game_id),
        }]
    } else {
        vec![Mutation::ClientError {
            client,
            message: "The room you attempted to join lo longer exists".to_string(),
        }]
    }
}

fn room_update_settings(
    game_id: GameId,
    settings: GameSettings,
    client: ClientId,
    lobby_state: &LobbyState,
) -> Vec<Mutation> {
    let game = if let Some(game) = lobby_state.games.get(&game_id) {
        game
    } else {
        return vec![Mutation::ClientError {
            client,
            message: format!(
                "Attempted to change settings of a game that doesn't exist: {}",
                game_id
            ),
        }];
    };

    if !game.players.contains(&client) {
        return vec![Mutation::ClientError {
            client,
            message: format!(
                "You must be a member of a game to change it's settings {}",
                game_id
            ),
        }];
    }

    return vec![Mutation::LobbyMutation(LobbyMutation {
        game_id: game_id,
        mutation_type: LobbyMutationType::UpdateRoomSettings (settings),
    })];
}

pub fn handle_lobby_event(
    ev: LobbyEvent,
    client: ClientId,
    lobby_state: &LobbyState,
) -> Vec<Mutation> {
    match ev {
        LobbyEvent::RequestRooms => {
            vec![]
        }
        LobbyEvent::JoinRoom(game_id) => player_join_room(game_id, client, lobby_state),
        LobbyEvent::UpdateGameSettings(game_id, settings) => {
            room_update_settings(game_id, settings, client, lobby_state)
        }
        LobbyEvent::CreateRoom => create_room(),
    }
}
