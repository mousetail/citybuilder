use crate::chat::send_chat_message;
use crate::events::lobby_events::handle_lobby_event;
use crate::ServerState;
use shared::events::client_event::ClientEvent;
use shared::client_location::ClientLocation;
use shared::client_state::ClientId;
use shared::Mutation;

pub(super) fn handle_client_event(
    event: ClientEvent,
    client: ClientId,
    server_state: &ServerState,
) -> Vec<Mutation> {
    match event {
        ClientEvent::LobbyEvent(ev) => handle_lobby_event(ev, client, &server_state.lobby_state),
        ClientEvent::SendChatMessage(msg) => send_chat_message(msg),
        ClientEvent::EnterLobby => vec![Mutation::MoveClient {
            client,
            destination: ClientLocation::InLobby,
        }],
        ClientEvent::EnterGameLobby(game_id) => {
            if server_state.lobby_state.games.contains_key(&game_id) {
                vec![Mutation::MoveClient {
                    client,
                    destination: ClientLocation::InGameLobby(game_id),
                }]
            } else {
                vec![Mutation::ClientError {
                    client,
                    message: format!("Cannot find game {}", game_id),
                }]
            }
        }
    }
}
