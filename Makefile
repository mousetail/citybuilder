serve:
	cp target/wasm32-unknown-unknown/debug/ui-client.wasm dist/ui-client.wasm
	wasm-bindgen --target web dist/ui-client.wasm --out-dir dist
	cp static/* dist
	cd dist && python -m http.server

