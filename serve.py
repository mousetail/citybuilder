import http.server
import threading
import time
import os.path
import shutil
import subprocess

filename = "target/wasm32-unknown-unknown/debug/ui-client.wasm"

def copy_file():
	shutil.copyfile(filename, "dist/ui-client.wasm")
	subprocess.check_call(["wasm-bindgen", "--target", "web", "dist/ui-client.wasm", "--out-dir", "dist"])

def copy_file_if_moved():
	copy_file()
	date_modified = os.path.getmtime(filename)
	print(f"Start watching at {time.ctime(date_modified)}")
	while running:
		new_date_modified = os.path.getmtime(filename)
		if new_date_modified > date_modified:
			print(f"Copying file, last modified {time.ctime(new_date_modified)}")
			copy_file()
			date_modified = new_date_modified
		else:
			time.sleep(0.1)


class RequestHandler(http.server.SimpleHTTPRequestHandler):
    def send_file(self, filename, content_type=None):

        with open(filename, mode='rb') as f:
            fs = os.fstat(f.fileno())

            if content_type == None:
                content_type = self.guess_type(filename)

            self.send_response(200, "Ok")
            self.send_header("Content-Type", content_type)
            self.send_header("Content-Length", str(fs.st_size))
            self.end_headers()

            if self.command !='HEAD':
                self.copyfile(f, self.wfile)

    def send_error(self, code, message=None, explain=None):
        if code != 404:
            return super().send_error(code, message, explain)

        path = self.translate_path(self.path)
        print(path)
        if path == os.path.join("static","ui-client.js"):
            self.send_file("dist/ui-client.js")
        elif path == os.path.join("static","ui-client_bg.wasm"):
            self.send_file("dist/ui-client_bg.wasm")
        elif path == os.path.join("static","ui-client_bg.js"):
            self.send_file("dist/ui-client_bg.js")
        else:
            self.send_file("static/index.html", "text/html")

class Server(http.server.HTTPServer):
    def finish_request(self, request, client_address):
        self.RequestHandlerClass(request, client_address, self,
                                     directory="static")

def run(server_class=http.server.HTTPServer, handler_class=http.server.BaseHTTPRequestHandler):
    server_address = ('', 8000)
    print(f"Serving on f{server_address}")
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

running = True

if __name__=="__main__":
    try:
        thread = threading.Thread(target=copy_file_if_moved).start()
        run(handler_class = RequestHandler, server_class = Server)
    finally:
        running = False
